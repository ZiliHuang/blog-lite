const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const knex = require('knex')
const path = require('path');
const app = express();

app.use(cors());

app.use(bodyParser.json());

const db = knex({
  client: 'mysql',
  connection: {
    host : '127.0.0.1',
    user : 'root',
    password : '',
    database : 'myproject'
  }
});


app.get('/login', (req, res) => {
  const { username, password }= req.body;
  db.select('*').from('users').where('username', '=', username, 'AND', 'password', '=', password)
  .then (user => {
    res.json(user);
  })
  .catch(err => res.json(err, ' no such user or wrong password'))
})



app.post('/register', (req, res) => {
  //check for username duplication needed  
  const { username, password }=req.body;
  db('users').insert({
    username: username,
    password: password
  })
  .then(user => { res.json(user);
  })
  .catch(err => res.josn(err, 'unable to register'))
});


app.get('/', (req, res)=> {
  db.select('*').from('articles')
  .then(articles => {
    res.json(articles);
  })
  .catch(err => res.status(400).json(err, 'error'))
});

app.post('/new_article', (req, res) => {
  const { title, content } = req.body;
  db('articles')
    .insert({
      title: title,
      content: content
  })
    .then(id => {
      res.json({id: id[0], title, content})
  })          
    .catch(err => res.status(400).json(err, 'unable to create a new article'))
});

app.get('/show_article/:id', (req, res) => {
  const  id  =req.params.id;
 db.select('*').from('articles').where('id', '=', id)
  .then(article => {
    res.json(article);
  })
});

app.put('/edit_article/:id', (req, res) => {
  const { title, content } = req.body;
  const  id =req.params.id;
  db('articles').where('id', '=', id)
  .returning(['id','title','content'])
  .update({
    title: title,
    content: content
  })
  .then(article => { res.json({id: id, title: title, content: content})
  })
  .catch(err => res.status(400).json('could not edit'))
});

app.delete('/delete_article/:id', (req, res) => {
  const  id  = req.params.id;
  db('articles').where('id', '=', id)
  .del()
  .then(res.status(200).json(req.params.id))
  .catch(err => res.status(400).json('wrong id to delete', err))
});


app.listen(3005, ()=> {
  console.log('app is running on port 3005');
});

import React, { Component } from 'react';
import Navbar from '../components/Navbar/Navbar';
import { connect } from 'react-redux';

import { setArticleList, fetchArticles, editArticle, newArticle } from '../actions/actions';
import {  BrowserRouter, Router, Route, Switch, Link } from "react-router-dom";
import NewArticle from '../components/Articles/NewArticle';
import Articles from '../components/Articles/Articles';
import ShowArticle from '../components/Articles/ShowArticle';
import EditArticle from '../components/Articles/EditArticle';
import  HomePage  from '../components/HomePage/HomePage';
import { LoginPage } from '../components/LoginPage/LoginPage';
import { RegisterPage } from '../components/RegisterPage/RegisterPage';
import { PrivateRoute } from '../components/PrivateRoute';
import { alertActions } from '../actions/alertActions';
import { history } from '../common/history';




class App extends Component {
	render(){
		console.log(this.props.articles)
		return(
			<BrowserRouter>
				<div >
					<Navbar />
					<Switch>
			          <Route exact path="/" component={ HomePage} />
			          <Route exact path="/new_article" component={NewArticle}/>
			          <Route
			                 exact path={"/show_article/:id"}
			                 render={({ match }) => <ShowArticle article = {this.props.articles.filter( article => article.id === parseInt(match.params.id), 10)[0]} />}
			          />
		              <Route 
		                  exact path={"/edit_article/:id"} 
		                  render={({ match }) => <EditArticle article = {this.props.articles.filter( article => article.id === parseInt(match.params.id), 10)[0]} />}
		                />
			        </Switch>
				</div>
			</BrowserRouter>
		)
	}
}



const mapStatetoProps = state => {
	return {
		articles: state.articleReducer.articles
	}
}

export default connect(mapStatetoProps, null)(App);


// <Route exact path="/" render={({ props }) => <Articles articles={this.props.articles} />} />

import React, { Component } from 'react';

import { Link } from "react-router-dom";
class Navbar extends Component {
	render(){
		return (
			<div>
				<nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-3">
					<div className="container">
						<a href="/" className="navbar-brand f3 dim balck pa3 pointer">Incudy</a>
						<button className="navbar-toggler" type="button" data-toggle="collpase" data-target="#mobile-nav">
							<span className="navbar-toggle-icon"></span>
						</button>
						<div className="collapse navbar-collapse" id="mobile-nav">
							<ul className="navbar-nav ml-auto">
								<Link to="/" className="white f3 link underline pa3 pointer">Home </Link>
								<Link to="/new_article" className="white f3 link  pa3 underline pointer">New Article </Link>
							</ul>
						</div>
					</div>
				</nav>

			</div>
		)
	}
}
export default Navbar;

// id={parseInt(match.params.id)}





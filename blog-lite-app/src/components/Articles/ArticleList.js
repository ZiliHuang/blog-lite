import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import {  delArticle, requestArticles, receiveArticles, setArticleList, fetchArticles } from '../../actions/actions';



class ArticleList extends Component {
  render() {
    if(this.props.articles){
      return(
        <div className="container-fluid">
       <table id="articles" className="tc table table-striped">
          <thead>
            <tr>
              <th>Article Title</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.props.articles.map((article, i) => (
              <tr key={article.id}>
                <td>{ article.title }</td>
                   <td>
                      <Link to= {"/show_article/"+article.id} className="btn btn-dark">Show</Link>
                      <Link to={'/edit_article/'+article.id} className="btn btn-light">Edit</Link>
                      <button
                        onClick={ () => { 
                          if (window.confirm('Are you sure you wish to delete this article?')){ 
                             this.props.onDeleteArticle(article.id)}
                          }
                        }
                        className="btn btn-danger delete-article"> Delete </button>
                   </td>
              </tr>
            ))}
          </tbody> 
        </table>
        </div>
    )}
      return(<h1> No articles found </h1>)
      }
}

const mapStateToProps = state => {
  return{
    articles: state.articleReducer.articles,
  }
}


const mapDispatchToProps = (dispatch) => {
  return ({
      onDeleteArticle: (id) => dispatch(delArticle(id))
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList);


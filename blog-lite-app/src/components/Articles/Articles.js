import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import {  delArticle, requestArticles,receiveArticles, setArticleList, fetchArticles } from '../../actions/actions';


const mapStatetoProps = state => {
  console.log(state)
  return{
    articles: state.articles
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchArticles,
    delArticle
  }, dispatch)
}

class Articles extends Component {

  onDeleteClick = (id) => {
    this.props.delArticle(id);
  }

  showArticles = () => {
    console.log(this.props.fetchArticles())
    this.props.fetchArticles();
  }

  componentDidMount(){
    if(this.props.articles){
      return(
        <div className="container-fluid">
       <table id="articles" className="tc table table-striped">
          <thead>
            <tr>
              <th>Article Title</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.props.articles.map((article, i) => (
              <tr key={article.id}>
                <td>{ article.title }</td>
                   <td>
                      <Link to= {"/show_article/"+article.id} className="btn btn-dark">Show</Link>
                      <Link to={'/edit_article/'+article.id} className="btn btn-light">Edit</Link>
                      <Link 
                        to={'/'} 
                        onClick={ () => { 
                          if (window.confirm('Are you sure you wish to delete this article?')){ 
                             this.onDeleteClick(article.id)}
                          }
                        }
                        className="btn btn-danger delete-article" 
                        data-id="{ article.id }">
                        Delete
                      </Link>
                   </td>
              </tr>
            ))}
          </tbody> 
        </table>
        </div>
    )}
      return(
        <div>
             <button onClick={this.showArticles()}
              className="btn btn-danger delete-article" 
              data-id="{ article.id }">
              Show Article List
            </button>
        </div>
        )
    }

  render (){
    console.log(this.props.articles)
    if(this.props.articles){
      return(
        <div className="container-fluid">
       <table id="articles" className="tc table table-striped">
          <thead>
            <tr>
              <th>Article Title</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.props.articles.map((article, i) => (
              <tr key={article.id}>
                <td>{ article.title }</td>
                   <td>
                      <Link to= {"/show_article/"+article.id} className="btn btn-dark">Show</Link>
                      <Link to={'/edit_article/'+article.id} className="btn btn-light">Edit</Link>
                      <Link 
                        to={'/'} 
                        onClick={ () => { 
                          if (window.confirm('Are you sure you wish to delete this article?')){ 
                             this.onDeleteClick(article.id)}
                          }
                        }
                        className="btn btn-danger delete-article" 
                        data-id="{ article.id }">
                        Delete
                      </Link>
                   </td>
              </tr>
            ))}
          </tbody> 
        </table>
        </div>
    )}
      return(
        <div>
               <button onClick={ 
                  this.showArticles()
               }
              className="btn btn-danger delete-article" 
              data-id="{ article.id }">
              Show Article List
                      </button>

        </div>
        )
  }
  
}

export default connect(mapStatetoProps, mapDispatchToProps)(Articles);

// <Route exact path={"/edit_article/"+this.state.articles[0].id} render ={(props) => ({EditArticle})} />
// onClick={() => onRouteChange ("/article/{ articles.id") } 



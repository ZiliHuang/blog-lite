import React from 'react';
import { newArticle, setArticleList } from '../../actions/actions';
import { connect } from 'react-redux';


class NewArticle extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			title:'',
			content:'',
		}
	}

	onTitleChange = (event) => {
		this.setState({title: event.target.value});
	}

	onContentChange = (event) => {
		this.setState({content: event.target.value});
	}

	handleSubmit = () => {
		this.props.onNewArticle(this.state.title, this.state.content)
	}

	render() {
		return (
		  <div>
		    <label className='db fw6 lh-copy f6' >
		      Title:
		      <input 
		      	type="text" 
		      	className='form-control' 
		      	// value={} 
		      	onChange={this.onTitleChange} 
		      />
		    </label>
		     <label className='db fw6 lh-copy f6' >
		      Content:
		      <textarea 
		      	type="text" 
		      	className='form-control' 
		      	rows='10' 
		      	// value={} 
		      	onChange={this.onContentChange} 
		      />
		    </label>
		    <input 
		    	className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer"
		    	type="submit" 
		    	value="Submit" 
		    	onClick={this.handleSubmit}
		    />
		  </div>
		);
	}
}


const mapDispatchToProps = (dispatch) => {
	return {
		 onNewArticle: (title, content) => dispatch(newArticle(title, content))
		// newArticle
	}
}

export default connect(null, mapDispatchToProps)(NewArticle);
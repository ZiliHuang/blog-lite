import React from 'react';

const ShowArticle = ( {article} ) => {
	return (
	<div>
		<div>
			<h1 className='tc'>{article.title}</h1>
		</div>
		<div>
			<p className='tc'>{article.content}</p>
		</div>
	</div>
	);
};
export default ShowArticle;

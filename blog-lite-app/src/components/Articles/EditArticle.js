import React, { Component } from 'react';
import { editArticle } from '../../actions/actions';
import { connect } from 'react-redux';



class EditArticle extends Component {
	constructor(props) {
		super(props);
		this.onTitleChange = this.onTitleChange.bind(this);
		this.onContentChange = this.onContentChange.bind(this);
		this.state = {
			title: this.props.article.title,
			content: this.props.article.content
		}
	}

	onTitleChange(event){
		this.setState({title: event.target.value});
	}

	onContentChange(event){
		this.setState({content: event.target.value});
	}


	handleEdit = () =>{
	     this.props.onEditArticle(this.props.article.id, this.state.title, this.state.content)
	}

	render(){
 	return (
		  <div>
		    <label className='db fw6 lh-copy f6' htmlFor='title'>
		      Title:
		      <input type="text" className='form-control' onChange={this.onTitleChange} value={this.state.title} />
		    </label>
		     <label className='db fw6 lh-copy f6'  htmlFor='content'>
		      Content:
		      <textarea type="text" className='form-control' rows='10' onChange={this.onContentChange} value={this.state.content} />
		    </label>
		    <input 
		    	className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer"
		    	type="submit" 
		    	value="Edit" 
		    	onClick={this.handleEdit}
		    />
		  </div>
	  )
 	}
}



const mapDispatchToProps = (dispatch) => {
  return {
      onEditArticle: (id, title, content) => {dispatch(editArticle(id, title, content))}
    }
}

export default connect(null, mapDispatchToProps)(EditArticle);
import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import {  delArticle, requestArticles,receiveArticles, setArticleList, fetchArticles } from '../../actions/actions';
import ArticleList from '../Articles/ArticleList'


class HomePage extends Component {
  constructor() {
    super()
    this.state = {
      displayArticles: false
    }
  }

  displayArticleList = () => {
    this.setState({
      displayArticles: !this.state.displayArticles
    })
  }

  render (){
    return( <ArticleList />)
    // return (
    //   <div>
    //     <button className="btn btn-dark" onClick={this.displayArticleList}> Show all articles </button>
    //     {this.state.displayArticles && <ArticleList />}
    //   </div>
    //   )    
  }
}
export default HomePage;

// <Route exact path={"/edit_article/"+this.state.articles[0].id} render ={(props) => ({EditArticle})} />
// onClick={() => onRouteChange ("/article/{ articles.id") } 



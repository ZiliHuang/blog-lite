import { combineReducers } from 'redux';
import React from 'react';
import {articleReducer} from './articleReducer';
import {alert} from './alertReducer';
import {login} from './loginReducer';
import {registration} from './registrationReducer';

const rootReducer = combineReducers({
	articleReducer, alert, login, registration
})

export default rootReducer;

import { 
	articlesConstants
 } from '../common/constants';

 import { REQUEST_ARTICLES_FAILED, REQUEST_ARTICLES_SUCCESS, REQUEST_ARTICLES_PENDING} from '../common/constants';


const { SHOW_USER_ARTICLES,
	CURRENT_ARTICLE_LIST, 
	UPDATE_ARTICLE_LIST,
	DELETE_ARTICLE_PENDING,
	DELETE_ARTICLE_SUCCESS,
	DELETE_ARTICLE_FAILED,
	EDIT_ARTICLE_PENDING,
	EDIT_ARTICLE_SUCCESS,
	CREATE_ARTICLE_PENDING,
	CREATE_ARTICLE_SUCCESS,
	CREATE_ARTICLE_FAILED,
} =articlesConstants;

// my staet might look like this
const initialState= {
	articles: [],
	users:{
		user:{
			//id: user id
			id:'',
			name:'',
			password:'',
		}
	},
	userArticles: {
		article:{
			//@id: article id
			id:'',
			title: '',
			content: '',
			//@author: user id
			author:'',
		}
	},
	articleList:{
		isPending: false,
		articles:[],
		error: '',
	},
	postArticle:{
		isPending: false,
		items:[],
		error: '',
		author:'',
	}
}

export const articleReducer = (state = initialState, action={}) => {
	switch(action.type){
		case REQUEST_ARTICLES_PENDING:
			return Object.assign({}, state, {isPending: true})
		case REQUEST_ARTICLES_SUCCESS:
			return Object.assign({}, state, {
				articles: action.payload, isPending: false
			})
		case REQUEST_ARTICLES_FAILED:
			return Object.assign({}, state, { error: action.payload})
		case EDIT_ARTICLE_PENDING:
			return Object.assign({}, state, {isPending: true})
		case EDIT_ARTICLE_SUCCESS:
			return Object.assign({}, state, 
					{articles: state.articles.map((article) => {
						if(article.id === parseInt(action.payload.id)){
							return article= {id:parseInt(action.payload.id), title: action.payload.title, content: action.payload.content}
						}
						return article
					}) 
				})
		case REQUEST_ARTICLES_FAILED:
			return Object.assign({}, state, {error: action.payload, isPending: false})
		case CREATE_ARTICLE_PENDING:
			return Object.assign({}, state, {isPending: true})
		case CREATE_ARTICLE_SUCCESS:   //Create Article fehler
			return Object.assign({}, state, {articles: state.articles.concat(action.payload), isPending: false})
		case CREATE_ARTICLE_FAILED:
			return Object.assign({}, state, {error: action.payload, isPending: false})
		case DELETE_ARTICLE_PENDING:
			return Object.assign({}, state, {isPending: true})
		case DELETE_ARTICLE_SUCCESS:
			return Object.assign({}, state, {articles: state.articles.filter(article => article.id !== parseInt(action.payload))})
		case DELETE_ARTICLE_FAILED:
			return Object.assign({}, state, {error: action.payload, isPending: false})
		default:
			return state;
	}
}
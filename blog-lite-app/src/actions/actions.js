//sync

import { 
	articlesConstants
} from '../common/constants';

import { REQUEST_ARTICLES_FAILED, REQUEST_ARTICLES_SUCCESS, REQUEST_ARTICLES_PENDING} from '../common/constants';

const { 
	SHOW_USER_ARTICLES,
	CURRENT_ARTICLE_LIST, 
	UPDATE_ARTICLE_LIST,
	DELETE_ARTICLE_PENDING,
	DELETE_ARTICLE_SUCCESS,
	DELETE_ARTICLE_FAILED,
	EDIT_ARTICLE_PENDING,
	EDIT_ARTICLE_SUCCESS,
	EDIT_ARTICLE_FAILED,
	CREATE_ARTICLE_PENDING,
	CREATE_ARTICLE_SUCCESS,
	CREATE_ARTICLE_FAILED,
	} 
	=articlesConstants;




const updateArticleList = (articles) => ({
	type: UPDATE_ARTICLE_LIST,
	articles
})

const showUserArticle = (articles, userId) => ({
	type: SHOW_USER_ARTICLES,
	articles,
	userId
})

const requestArticles = (isPending) => ({
	type: REQUEST_ARTICLES_PENDING,
	isPending
})

const receiveArticles = (payload, isPending) => ({
	type: REQUEST_ARTICLES_SUCCESS,
	payload,
	isPending
})

const fetchArticlesError = (isPending, hasError) => ({
	type: REQUEST_ARTICLES_FAILED,
	isPending,
	hasError
})


export const fetchArticles = () => (dispatch) => {
	dispatch(requestArticles());
	 fetch('http://localhost:3005/', {
	      method: 'GET',
	      headers: {
	      'Content-type': 'application/json'
	      }
	    })
	    .then(response => response.json())
	    .then(json => dispatch(receiveArticles(json)))
	    .catch(err => dispatch(fetchArticlesError()));
}



export const editArticle = (id, title, content) => (dispatch) => {
	dispatch({ type: REQUEST_ARTICLES_PENDING });
		 fetch('http://localhost:3005/edit_article/'+id, {
	      method: 'put',
	      headers: {'Content-Type': 'application/json'},
	      body: JSON.stringify({
	        title: title,
	        content: content
	      	})
	     })
	      .then(response => response.json())
	      .then(data => dispatch({ type: EDIT_ARTICLE_SUCCESS, payload: data}))
	      .catch(err => dispatch({ type: EDIT_ARTICLE_FAILED, payload: err}));
}


export const newArticle = (title, content) => (dispatch) => {
	dispatch({ type: CREATE_ARTICLE_PENDING });
	 fetch('http://localhost:3005/new_article', {
      method: 'post',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        title: title,
        content: content
      })
    })
      .then(response => response.json())
      .then(data => dispatch({ type: CREATE_ARTICLE_SUCCESS, payload: data}))  //ehler ist hier
      .catch(err => dispatch({ type: CREATE_ARTICLE_FAILED, payload: err}));
}


export const delArticle = (id) => (dispatch) => {
	dispatch({ type: DELETE_ARTICLE_PENDING });
	    fetch('http://localhost:3005/delete_article/' + id, {
	        method: 'delete'
	      })
	    .then(response => response.json())
     	.then(data => dispatch({ type: DELETE_ARTICLE_SUCCESS, payload: data}))
        .catch(err => dispatch({ type: DELETE_ARTICLE_FAILED, payload: err}));
}




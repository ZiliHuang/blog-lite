import { userConstants } from '../common/constants';
import { alertActions } from './alertActions';

export const userActions = {
	login,
	logout,
	register,
	getAll,
	delete: _delete
}

const login = (username, password) => {
	return dispatch => {
		dispatch(request({ username }));

		userService.login(username, password)
		.then( user=> {
			dispatch(success(user));
			history.push('/');
			},
			error => {
				dispatch(failure(error.toString()));
				dispatch(alertActions.error(error.toString()));
		});
	};
	const request = (user) => ({ type: userConstants.USERS_LOGIN_REQUEST, user })
	const success = (user) => ({ type: userConstants.USERS_LOGIN_SUCCESS, user })
	const failure = (error) => ({ type: userConstants.USERS_LOGIN_FAILURE, error })
}

const logout = () => {
	userService.logout();
	return { type: userConstants.LOGOUT};
}

const register = (user) => {
	return dispatch => {
		dispatch(request(user));

		userService.register(user)
		.then( user =>{
			dispatch(success());
			history.push('/login');
			dispatch(alertActions.success('Registered'));
		},
		error => {
			dispatch(failure(error.toString()));
			dispatch(alertActions.error(error.toString()));
		}
		)
	}

	function request(user) { return { type: userConstants.USERS_REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.USERS_REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.USERS_REGISTER_FAILURE, error } }
}


// function getAll() {
//     return dispatch => {
//         dispatch(request());

//         userService.getAll()
//             .then(
//                 users => dispatch(success(users)),
//                 error => dispatch(failure(error.toString()))
//             );
//     };

//     function request() { return { type: userConstants.GETALL_REQUEST } }
//     function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
//     function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
// }

// // prefixed function name with underscore because delete is a reserved word in javascript
// function _delete(id) {
//     return dispatch => {
//         dispatch(request(id));

//         userService.delete(id)
//             .then(
//                 user => dispatch(success(id)),
//                 error => dispatch(failure(id, error.toString()))
//             );
//     };

//     function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
//     function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
//     function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
// }
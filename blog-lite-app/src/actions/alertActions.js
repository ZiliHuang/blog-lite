import { alertConstants } from '../common/constants';

export const alertActions = {
	success,
	clear,
	error
}

const success = (message) => ({ type: alertConstants.ALERT_SUCCESS, message});

const clear = (message) => ({ type: alertConstants.ALERT_CLEAR, message});

const error = (message) => ({ type: alertConstants.ALERT_ERROR, message});